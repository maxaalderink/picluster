#Scripts
All scripts were created using Ansible 2.8 and Docker 19.03.

##Preparations

###Node:install OS
To do anything with your SBC you must first have an operating system running on it.
I prefer to use [Armbian](https://www.armbian.com/) and the scripts were tested on Focal Server(kernel 5.10) variants of this OS.
Read how to setup and install Armbian here: [Armbian Quick Start Guide](https://docs.armbian.com/User-Guide_Getting-Started/).

###Node:add ansible user
Ssh to the node using the user you created during the setup in the previous step.
Create a new user 'ansible', add it to the sudoers list, and set a password.
```shell
ssh <user>@<node_address>
useradd -m ansible
usermod -aG sudo ansible
passwd ansible
```
Then edit the sudo configuration with the command:
```shell
sudo visudo
```
Add the following line at the bottom.
```
ansible ALL=(ALL) NOPASSWD: ALL
```
This allows the Ansible user to execute sudo commands without requesting a password.

###Local:create and register ansible key pair
Create public/private key pair for 'ansible' to use.
Now authorize the key to login as 'ansible' at the node (this should require a password).
Lastly try to ssh to the node as 'ansible' (this should no longer require a password).
```shell
ssh-keygen -t rsa -f ~/.ssh/ansible
ssh-copy-id -i ~/.ssh/ansible ansible@<node_address>
ssh ansible@<node_address>
```

###Local:install and configure Ansible
First install Ansible:
```shell
sudo apt update
sudo apt install ansible
```
For Ansible to use the 'ansible' key pair some configuration change need to be made.
Edit the config file:
```shell
sudo nano /etc/ansible/ansible.cfg
```
Find, uncomment and change the following lines:
```
remote_user = ansible
private_key_file = ~/.ssh/ansible
```
Ansible will now use the 'ansible' to ssh to the node and execute commands.

In addition to that you might want to change the following line too:
```
forks = 10
```
This allows ansible to connect to 10 node simultaneously, speeding up some processes.

###(Optional)Node:disable password authentication
To increase security you could disable ssh authentication using a password for the 'ansible' user.
Ssh to the node and edit the ssh config:
```shell
ssh ansible@<node_address>
nano /etc/ssh/sshd_config
```
Then in the '# Authentication:' section add the following:
```
Match User ansible
PasswordAuthentication no
Match all
```
To make the changes effective reload ssh:
```shell
sudo systemctl reload ssh
```

###(Optional)Node:change hostname
I prefer to set the hostname of each node to equal the name in the ansible inventory.
```shell
sudo hostnamectl set-hostname <inventory_node_name>
```

###(Optional)Node:disable IPv6
I had some issues with nodes when connected to specific routers.
This was resolved by disabling IPv6.
Run the following command:
```shell
nmtui-edit
```
Then navigate to 'edit' -> 'IPv6 Configuration' and set 'Automatic' to 'Ignore'.

###(Optonal)Node:Hardware configuration
Some boards are not running at maximum performance by default.
Check which options are available by runnin the command:
```shell
sudo armbian-config
```
and navigation to 'hardware' -> 'cpu'.
