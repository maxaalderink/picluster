# Parts

- 6 x NanoPi Neo2 1GB
- 6 x microSD card 32 GB
- 1 x Mean Well 75w 5v PSU
- 1 x Scythe Slip Stream Slim fan 120 x 120 x 12 mm
- 1 x Edimax 8 port Gigabit switch
- 6 x UTP flat-cable 10 cm
- 2 x UTP mount
- 2 x USB-A mount
- 1 x 220v EURO plug mount
- 4 x case fan screws
- 4 x m2.5 rod 160 mm
- 36 x m2.5 torx bolt 8 mm
- 102 x m2.5 hex nut
- 12 x m3 torx bolt 8 mm
- 4 x m3 hex stand-off 10 mm
- 4 x m5 rubber feet
- Acryl 600 x 300 x 3 mm

## notes
It isn't essential to get exactly these parts if you want to build your own cluster.
A lot is up to personal preference so ill try and list my reasoning:

- I choose the NanoPi Neo2 because it had everything I needed and not that much more.
This compact, headless SBC has a 64bit QuadCore ARM CPU, 1 GB RAM 1 Gbit Ethernet is perfect in my eyes.
Choosing a different SBC will probably have different dimensions and will require more alterations to the blueprints.
- Based on the listed power usage I calculated that I needed a 75W PSU, but in practice I have not seen the power draw exceed 25 watt so 
 it is save to go with a smaller PSU.
- The fan that I used is half the height of a standard 120mm fan but a regular would probably also fit.  
More important is that you look at the minimum startup voltage, this should be 5v or lower so you don't need to have a secondary 9 or 12 
volt line.
- The only thing special about this ethernet switch is that is accepts a 5v power intake so it can be hooked up directly to the PSU.
- You really need the flexibility of flat-cables for the UTP connections but beside that there is nothing special about them.
- Initially I did't have USB connections exposed but since I had room left I thought I might add them for extra storage or something.
- Obviously you don't need to use torx screws but I do recommend those or hex screws above regular philips heads.
Both torx and hex screws make it easier to apply the right amount tension without cracking the acrylics.
- M3 nuts and bolts are easier to find the m2.5 but since I already needed them for the NanoPi's I used them where ever possible.
If you choose a different SBC you might be able to use m3 everywhere.